var createError = require('http-errors');
var express = require('express');
var path = require('path');


var app = express();



app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'build')));

app.use('/', function(req, res) {
    res.sendFile(path.join(__dirname + './build/index.html'));
});


app.listen(3000);

module.exports = app;
