import React, { useState } from "react";
import "./SideBar.css";
import './style.scss';
import { Nav } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faGlobeAsia,
  faBookReader,
  faBookMedical,
  faUser,
  faAmbulance,
  faBars,
  faGraduationCap,
  faAddressCard,
} from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

export default function SideBar(props) {
 
  return (
    <div className="side-bar-wrapper" >
     {/*  <span onClick = { toggleSidebar } className = 'toggle-sidebar'><FontAwesomeIcon icon = { faBars } /></span> */}
      <div className="sidebar-header" >
        <h3>
          <span>
            <FontAwesomeIcon icon={faGraduationCap} size="sm" /> UIT Course
          </span>
        </h3>
      </div>
      <Nav className="nav-wrapper">
        <Nav.Link>
          <Link to="/">
            <span>
              <FontAwesomeIcon className="icon-sidebar" icon={faBookReader} />{" "}
              Tổng quan
            </span>
          </Link>
        </Nav.Link>
        <Nav.Link>
          <Link to="/category">
            <span>
              <FontAwesomeIcon className="icon-sidebar" icon={faBookMedical} />{" "}
              Khoa
            </span>
          </Link>
        </Nav.Link>
        <Nav.Link>
          <Link to="/user">
            <span>
              <FontAwesomeIcon className="icon-sidebar" icon={faUser} /> User
            </span>
          </Link>
        </Nav.Link>
        <Nav.Link>
          <Link to="/course">
            <span>
              <FontAwesomeIcon className="icon-sidebar" icon={faBookReader} />{" "}
              Môn học
            </span>
          </Link>
        </Nav.Link>
        <Nav.Link>
          <Link to="/training-program">
            <span>
              <FontAwesomeIcon className="icon-sidebar" icon={faAmbulance} />{" "}
              Chương trình đào tạo
            </span>
          </Link>
        </Nav.Link>
        <Nav.Link>
          <Link to="/class">
            <span>
              <FontAwesomeIcon className="icon-sidebar" icon={ faAddressCard } />{" "}
                Lớp
            </span>
          </Link>
        </Nav.Link>
        <Nav.Link>
          <Link to="/manage-course-training-program">
            <span>
              <FontAwesomeIcon className="icon-sidebar" icon={ faAddressCard } />{" "}
              Môn học theo chương trình đào tạo
            </span>
          </Link>
        </Nav.Link>
      </Nav>
    </div>
  );
}
