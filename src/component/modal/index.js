import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect, useState } from 'react';

import './style.scss';

const ModalComponent = (props) => {

    const { open } = props;
    const { title , renderComponents } = props;

    function handleToggleModal(){
        open ? props.setOpen(false) : props.setOpen(true);
    }

    useEffect(() => {
        props.setOpen(props.open)
    }, [ props.open ])

    return (
        <div className = { open ? 'modal-container' : ' modal-container hide-modal' }>
            <div className= 'wrapper-modal' onClick = { handleToggleModal }>

            </div>
            <div className = 'modal-content'>
                <span onClick = { handleToggleModal } className = 'btn-close'><FontAwesomeIcon icon = { faTimes } /></span>
                <div className = 'title'>
                    { title }
                </div>
                <div className = 'components '>
                    { renderComponents }
                </div>
            </div>
           
        </div>
    );
};

export default ModalComponent;