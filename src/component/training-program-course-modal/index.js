import React, { useEffect, useState } from 'react';

import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import './style.scss';
import { connect } from 'react-redux';
import { ADD_TRAINING_PROGRAM_COURSE, GET_ALL_COURSE } from '../../config/ActionTypes';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
function TrainingProgramCourseModal(props) {
    const [type, settype] = useState('add');
    const { idTrainingProgram, _id } = props;
    const [data, setdata] = useState([
        {
            _id,
            idCourse: '',
            idTrainingProgram,
            isRequired: null,
            course_type:'',
            semester_can_learn:[],
            default_semester:'',
            can_learn_from_semester:''
        }
    ]);
    function updateData(index, value){
        const temp = data;
        temp[index] = { ...temp[index], ...value };
        setdata([...temp]);
    }

    function removeCourseInput(index){
        console.log(index);
        const temp = data.filter((item,idx) => idx != index );

        setdata([...temp]);
    }

    function handleSubmit(){
        if(type == 'add'){
            props.dispatch({
                type: ADD_TRAINING_PROGRAM_COURSE,
                data
            })
        }
      
    }

    useEffect(() => {
        settype(props.type);
        if(props.courses.length == 0){
            props.dispatch({
                type: GET_ALL_COURSE
            })
        }
    }, [  ]);

    console.log(data);

    function handleClick(index){
        data[index].specialize ?
        updateData(index, { specialize : null }) :
         updateData(index, { specialize: props.specializes[0]._id });
    }

    return (
        <div className = 'training-container'>
            <div className = 'modal-wrapper' onClick = { props.closeModal }>
            </div>
            <div className = 'training-program-course-modal'>
                <div className = 'title'>
                    Thêm môn học
                    <span onClick = { props.closeModal } className = 'btn-close'> <FontAwesomeIcon icon = { faTimes }/> </span>
                </div>
               
                {
                    data.map((item, index) => 
                        <div key = {  'training' + index } className = 'course'>
                            <div className = 'index'>
                                { index + 1 }
                            </div>
                            <div className = 'fields'>
                                <div className = 'field'>
                                    <label>
                                        Môn học 
                                    </label>
                                    <Autocomplete
                                    id="combo-box-demo"
                                    label = 'Tên môn học...'
                                    options={ props.courses }
                                    getOptionLabel={(option) => option.name + ' - ' + option.code }
                                    onChange = {(event, value) => updateData(index, { idCourse: value && value._id }) }
                                    fullWidth
                                    renderInput={(params) => 
                                        <TextField 
                                        placeholder = 'Tên môn học'
                                       
                                        {...params}  label="Tên môn học..." 
                                        variant="outlined" />
                                        
                                        }
                                    />
                                </div>
                                <div className = 'field'>
                                    <label>
                                        Được mở ở các học kì:
                                    </label>
                                    <TextField
                                        fullWidth
                                        value = { item.semester_can_learn }
                                        placeholder = ' Cách nhau bởi dấu phẩy'
                                        onChange = {(e) => updateData(index, { semester_can_learn: e.target.value })}
                                    />
                                </div>
                                <div className = 'field'>
                                    <label>
                                        Học kì mặt định:
                                    </label>
                                    <TextField
                                        fullWidth
                                        value = { item.default_semester }
                                        placeholder = ' học kì mặc '
                                        onChange = {(e) => updateData(index, { default_semester: e.target.value })}
                                    />
                                </div>
                                <div className = 'field'>
                                    <label>
                                        Có thể bắt đầu học từ học kì:
                                    </label>
                                    <TextField
                                        fullWidth
                                        value = { item.can_learn_from_semester }
                                        placeholder = ' học kì có thể bắt đầu học'
                                        onChange = {(e) => updateData(index, { can_learn_from_semester: e.target.value })}
                                    />
                                </div>
                                <div className = 'field'>
                                    <label>
                                        Môn tiên quyết 
                                    </label>
                                    <Autocomplete
                                    fullWidth
                                    id="combo-box-demo"
                                    label = 'Tên môn học...'
                                    options={ props.courses }
                                    getOptionLabel={(option) => option.name + ' - ' +  option.code }
                                    onChange = {(event, value) => updateData(index, { isRequired: value && value._id }) }
                                   
                                    renderInput={(params) => 
                                        <TextField 
                                        placeholder = 'Tên môn học'
                                        {...params}  label="Tên môn học..." 
                                        variant="outlined" />
                                        
                                        }
                                    />
                                </div>
                                <input checked = { Boolean(data[index].specialize) } onChange = {() =>  handleClick(index) } className = 'mr-2' type = 'checkbox' /><label>Môn chuyên ngành</label>
                                {
                                    data[index].specialize && 
                                    <div className = 'field'>
                                        <select onChange = {(e) => updateData(index, { specialize: e.target.value })}>
                                            {
                                                props.specializes.map((spe) => 
                                                    <option key = { spe._id } value = { spe._id } >{ spe.name }</option>
                                                )
                                            }
                                        </select>
                                    </div>
                                }
                            </div>
                            <div className = 'btn-remove'>
                                <span ><FontAwesomeIcon onClick = { () => removeCourseInput(index)} icon = { faTimes } /></span>
                            </div>
                        </div>
                    )
                }
                 <br />
                 <button onClick = { handleSubmit } className = 'btn btn-success ml-3 mr-3'>
                     Cập nhật
                 </button>
                <button 
                className = 'btn btn-success '
                onClick = { 
                    () =>
                    setdata(
                        [ ...data ,  {
                            _id,
                            idCourse: '',
                            idTrainingProgram,
                            isRequired: ''
                            }
                        ]) 
                    }> 
                Thêm 1 môn
               
                </button>
            </div>
        </div>
        
    );
}
const mapStateToProps = state => ({
    courses : state.course.data.allCourse,
    specializes: state.trainingProgram.specializes
});

const mapDispatchToProps = dispatch => ({
    dispatch
});
export default connect(mapStateToProps, mapDispatchToProps)(TrainingProgramCourseModal);