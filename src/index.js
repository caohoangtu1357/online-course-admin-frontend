import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './style.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';

import {createStore,applyMiddleware} from 'redux'
import {Provider} from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import reducer from './reducer/index';
import axios from 'axios';
import rootSaga from './saga/index';
import Bootstrap from 'bootstrap/dist/css/bootstrap.min.css';

axios.interceptors.request.use(request=>{
    request.headers["auth-token"]=localStorage.getItem("auth-token");
    return request;
});

axios.interceptors.response.use(response=>{
    console.log("response");
    return response;
});

const sagaMiddleware = createSagaMiddleware();
const store = createStore(reducer,applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);

const routing = (
    <Provider store={store}>
        <App/>
    </Provider>
);

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
