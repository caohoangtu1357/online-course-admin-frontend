import { GET_ALL_SCHOOL_YEAR_SUCCESS } from "../config/ActionTypes";

export const getAllSchoolYearSuccess = data => ({
    type: GET_ALL_SCHOOL_YEAR_SUCCESS, 
    data
})