
import {
    GET_LESSON_BY_ID_COURSE_SUCCESS,
    GET_LESSON_BY_ID_COURSE_FALSE
} from '../config/ActionTypes';

export function getLessonByIdCourseSuccess(data){
    return {type:GET_LESSON_BY_ID_COURSE_SUCCESS,data:data}
}
export function getLessonByIdCourseFalse(error){
    return {type:GET_LESSON_BY_ID_COURSE_FALSE,error:error}
}