import { ADD_TRAINING_PROGRAM_COURSE_SUCCESS, ADD_TRAINING_PROGRAM_SUCCESS, GET_ALL_TRAINING_PROGRAM_SUCCESS, GET_SPECIALIZES_BY_TRAINING_PROGRAM, GET_SPECIALIZES_BY_TRAINING_PROGRAM_SUCCESS, GET_TRAINING_PROGRAM_COURSES_SUCCESS, GET_TRAINING_PROGRAM_SUCCESS, REMOVE_TRAINING_PROGRAM_COURSE_SUCCESS } from "../config/ActionTypes";

export const getTrainingProgramsSuccess = data => ({
    type: GET_ALL_TRAINING_PROGRAM_SUCCESS, data
})

export const addTrainingProgramSuccess = data => ({
    type: ADD_TRAINING_PROGRAM_SUCCESS,
    data
})

export const getTrainingProgramCoursesSuccess = data => ({
    type: GET_TRAINING_PROGRAM_COURSES_SUCCESS, data
})

export const addTrainingProgramCourseSuccess = data => ({
    type: ADD_TRAINING_PROGRAM_COURSE_SUCCESS, data
})

export const getTrainingProgramSuccess = data => ({
    type: GET_TRAINING_PROGRAM_SUCCESS, data
})

export const removeTrainingProgramCourseSuccess = data => ({
    type: REMOVE_TRAINING_PROGRAM_COURSE_SUCCESS, data
});

export const getSpecializeSuccess = data => ({
    type: GET_SPECIALIZES_BY_TRAINING_PROGRAM_SUCCESS,
    data
})