import { ADD_CLASS_SUCCESS, DELETE_CLASS_SUCCESS, GET_ALL_CLASS_SUCCESS, GET_STUDENTS_BY_CLASS_SUCCESS, UPDATE_CLASS_SUCCESS, UPDATE_SCORE_STUDENT_SUCCESS } from "../config/ActionTypes";

export const getAllClassSuccess = data => ({
    type: GET_ALL_CLASS_SUCCESS,
    data
})

export const getStudentsByClassSuccess = data => ({
    type: GET_STUDENTS_BY_CLASS_SUCCESS,
    data
})

export const addClassSuccess  = data => ({
    type: ADD_CLASS_SUCCESS, 
    data
})

export const deleteClassSuccess = data => ({
    type: DELETE_CLASS_SUCCESS,
    data
})

export const updateClassSuccess = data => ({
    type: UPDATE_CLASS_SUCCESS, 
    data
})

export const updateScoreStudentSuccess = data => ({
    type: UPDATE_SCORE_STUDENT_SUCCESS,
    data
})