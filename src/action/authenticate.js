import {
    LOGIN_FAILURE,
    LOGIN_SUCCESS,

    LOGOUT_SUCCESS,
    LOGOUT_FAILURE,

    CHECK_IS_LOGGED_SUCCESS,
    CHECK_IS_LOGGED_FAILURE
} from '../config/ActionTypes';

export function loginSuccess(data){
    return{type:LOGIN_SUCCESS,data:data}
}
export function loginFailure(error){
    return{type:LOGIN_FAILURE,error:error}
}

export function logoutSuccess(data){
    return{type:LOGOUT_SUCCESS,data:data}
}
export function logoutFailure(error){
    return{type:LOGOUT_FAILURE,error:error}
}

export function checkIsLoggedSuccess(data){
    return{type:CHECK_IS_LOGGED_SUCCESS,data:data}
}
export function checkIsLoggedFailure(error){
    return{type:CHECK_IS_LOGGED_FAILURE,error:error}
}
