import { GET_ALL_MAJOR_SUCCESS } from "../config/ActionTypes";

export const getAllMajorSuccess = data => ({
    type: GET_ALL_MAJOR_SUCCESS,
    data 
})