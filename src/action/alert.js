import {
    CREATE_ALERT,
    STOP_ALERT
}from "../config/ActionTypes";

export function addAlert(message){
    return {type:CREATE_ALERT,message:message}
}

export function stopAlert(){
    return {type:STOP_ALERT}
}