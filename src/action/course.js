import {
  GET_ALL_COURSE,
  GET_ALL_COURSE_SUCCESS,
  GET_ALL_COURSE_FAILURE,
  GET_AN_COURSE,
  GET_AN_COURSE_SUCCESS,
  GET_AN_COURSE_FAILURE,
} from "../config/ActionTypes";

export function getAllCourseSuccess(data) {
  return { type: GET_ALL_COURSE_SUCCESS, data: data };
}
export function getAllCourseFailure(error) {
  return { type: GET_ALL_COURSE_FAILURE, error: error };
}

export function getAnCourseSuccess(data) {
  return { type: GET_AN_COURSE_SUCCESS, data: data };
}
export function getAnCourseFailure(error) {
  return { type: GET_AN_COURSE_FAILURE, error: error };
}
