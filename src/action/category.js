import {
    GET_ALL_CATEGORY,
    GET_ALL_CATEGORY_SUCCESS,
    GET_ALL_CATEGORY_FAILURE,

    GET_AN_CATEGORY,
    GET_AN_CATEGORY_SUCCESS,
    GET_AN_CATEGORY_FAILURE,

    UPDATE_CATEGORY,
    UPDATE_CATEGORY_SUCCESS,
    UPDATE_CATEGORY_FAILURE,

    ADD_CATEGORY,
    ADD_CATEGORY_FAILURE,
    ADD_CATEGORY_SUCCESS
} from '../config/ActionTypes';

export function updateCategorySuccess(data){
    return {type:UPDATE_CATEGORY_SUCCESS,data:data}
}
export function updateCategoryFailure(error){
    return {type:UPDATE_CATEGORY_FAILURE,error:error}
}


export function getAllCategorySuccess(data){
    return {type:GET_ALL_CATEGORY_SUCCESS,data:data}
}
export function getAllCategoryFailure(error){
    return {type:GET_ALL_CATEGORY_FAILURE,error:error}
}



export function getAnCategorySuccess(data){
    return {type:GET_AN_CATEGORY_SUCCESS,data:data}
}
export function getAnCategoryFailure(error){
    return {type:GET_AN_CATEGORY_FAILURE,error:error}
}


export function addCategorySuccess(data){
    return {type:ADD_CATEGORY_SUCCESS,data:data}
}
export function addCategoryFailure(error){
    return {type:ADD_CATEGORY_FAILURE,error:error}
}

