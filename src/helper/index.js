function formatDate(dateStr){
    const date = new Date(dateStr);

    return date.getDate() + '-' + (parseInt(date.getMonth()) + 1) + '-' + date.getFullYear();
}

export {
    formatDate
}