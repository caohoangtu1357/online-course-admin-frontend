import { GET_ALL_SCHOOL_YEAR_SUCCESS } from "../config/ActionTypes";

const initState = {
    list : []
}

export default function schoolYearReducer(state = initState , action){
    switch(action.type){
        case GET_ALL_SCHOOL_YEAR_SUCCESS :
            return { ...state, list : action.data };
        default:
            return state;
    }
}