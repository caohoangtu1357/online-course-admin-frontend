import { GET_ALL_MAJOR, GET_ALL_MAJOR_SUCCESS } from "../config/ActionTypes";

const initState = {
    list : []
}

export default function majorReducer(state = initState , action){
    switch(action.type){
        case GET_ALL_MAJOR_SUCCESS :
            return { ...state, list : action.data };
        default:
            return state;
    }
}