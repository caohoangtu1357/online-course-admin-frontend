import {
    GET_LESSON_BY_ID_COURSE_FALSE,
    GET_LESSON_BY_ID_COURSE_SUCCESS
 } from "../config/ActionTypes";
 
 const init = {
     data: 
     {
         lessonByIdCourse:[],
     },
     error: 
     {
        lessonByIdCourse:null,
     },
 };
 
 export default function lessonReducer(state= init, action){
     switch (action.type){
        case GET_LESSON_BY_ID_COURSE_SUCCESS:
            return {
                ...state,
                data:{
                    ...state.data,
                    lessonByIdCourse:action.data
                },
                error:{
                ...state.error,
                lessonByIdCourse:null
                }
            };
        
        case GET_LESSON_BY_ID_COURSE_FALSE:
            return {
                ...state,
                error:{
                ...state.error,
                lessonByIdCourse:action.error
                }
            };
    
         default:
             return state;
     }
 }