import {
  GET_ALL_CATEGORY_SUCCESS,
  GET_ALL_CATEGORY_FAILURE,
  GET_AN_CATEGORY_SUCCESS,
  GET_AN_CATEGORY_FAILURE,
  UPDATE_CATEGORY_SUCCESS,
  UPDATE_CATEGORY_FAILURE,
  ADD_CATEGORY_FAILURE,
  ADD_CATEGORY_SUCCESS,
} from "../config/ActionTypes";

const init = {
  data: {
    allCategory: [],
    category: {},
  },
  error: {
    allCategory: null,
    category: null,
  },
};

export default function categoryReducer(state = init, action) {
  switch (action.type) {
    //READ CATEGORY
    case GET_ALL_CATEGORY_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          allCategory: action.data,
        },
      };

    case GET_ALL_CATEGORY_FAILURE:
      return {
        ...state,
        error: {
          ...state.error,
          allCategory: action.error,
        },
      };

    case ADD_CATEGORY_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          category: action.data,
        },
        error: {
          ...state.error,
          category: null,
        },
      };
    case ADD_CATEGORY_FAILURE:
      return {
        ...state,
        error: {
          ...state.error,
          category: action.error,
        },
      };

    case UPDATE_CATEGORY_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          category: action.data,
        },
      };
    case UPDATE_CATEGORY_FAILURE:
      return {
        ...state,
        error: {
          ...state.error,
          category: action.error,
        },
      };

    case GET_AN_CATEGORY_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          category: action.data,
        },
      };
    case GET_AN_CATEGORY_FAILURE:
      return {
        ...state,
        error: {
          ...state.error,
          category: action.error,
        },
      };
    default:
      return state;
  }
}
