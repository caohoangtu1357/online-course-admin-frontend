import { ADD_TRAINING_PROGRAM_COURSE_SUCCESS, ADD_TRAINING_PROGRAM_SUCCESS, GET_ALL_TRAINING_PROGRAM_SUCCESS, GET_SPECIALIZES_BY_TRAINING_PROGRAM_SUCCESS, GET_TRAINING_PROGRAM_COURSES_SUCCESS, GET_TRAINING_PROGRAM_SUCCESS, REMOVE_TRAINING_PROGRAM_COURSE_SUCCESS } from "../config/ActionTypes";

const initState = {
    list : [],
    courses : [],
    current : {},
    specializes : []
}

export default function trainingProgramReducer (state = initState, action) {
    switch(action.type){
        case GET_ALL_TRAINING_PROGRAM_SUCCESS:
            return { ...state, list :  action.data };
        case ADD_TRAINING_PROGRAM_SUCCESS:
            return { ...state, list : [ ...state.list, action.data ] };
        case GET_TRAINING_PROGRAM_SUCCESS:
            return { ...state, current: action.data };
        case GET_TRAINING_PROGRAM_COURSES_SUCCESS:
            return { ...state, courses : action.data };
        case ADD_TRAINING_PROGRAM_COURSE_SUCCESS:
            return { ...state, courses: [ ...state.courses, ...action.data ]};
        case REMOVE_TRAINING_PROGRAM_COURSE_SUCCESS:
            const filter = state.courses.filter((item,idx) => idx !== action.data);
            return { ...state, courses: filter};
        case GET_SPECIALIZES_BY_TRAINING_PROGRAM_SUCCESS:
            return { ...state, specializes: action.data };
        default: 
            return state;
    }
}