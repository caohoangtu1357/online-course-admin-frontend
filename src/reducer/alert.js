import {
    CREATE_ALERT,
    STOP_ALERT
  } from "../config/ActionTypes";
  
  const init = {
    message:null
  };
  
  export default function errorReducer(state = init, action) {
    console.log("errror in reducer",action.message);
    switch (action.type) {
        case CREATE_ALERT:
            return {
            ...state,
            message:action.message
            };
        case STOP_ALERT:
          console.log("run reducer stop");
            return {
                ...state,
                message:null
            }
        default:
            return state;
    }
  }
  