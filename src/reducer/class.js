import { ADD_CLASS_SUCCESS, DELETE_CLASS_SUCCESS, GET_ALL_CLASS_SUCCESS, GET_STUDENTS_BY_CLASS_SUCCESS, UPDATE_CLASS_SUCCESS, UPDATE_SCORE_STUDENT } from "../config/ActionTypes";

const initState = {
    list : [],
    students : []
}


export default function classReducer(state = initState, action){
    switch(action.type){
        case GET_ALL_CLASS_SUCCESS : 
            return { ...state, list : action.data };
        case GET_STUDENTS_BY_CLASS_SUCCESS:
            return { ...state, students : action.data };
        case ADD_CLASS_SUCCESS: 
            return { ...state, list:  [ ...state.list, action.data ] };
        case UPDATE_CLASS_SUCCESS:
            const { data } = action;
            const temp = state.list;
            temp.forEach((item, index) => {
                if(item._id == data._id)
                {
                    temp[index] = data;
                }
            });
            return { ...state ,list : [ ...temp ] };
        case DELETE_CLASS_SUCCESS:
            return { ...state , list: state.list.filter(item => item._id != action.data.id ) };
        case UPDATE_SCORE_STUDENT:
            const students = state.students;
            students.forEach((item,index) => {
                if(item.idUser._id == action.data.idUser){
                    students[index]['score'] = action.data.score; 
                  
                }
            });
            return { ...state, students: [ ...students ]};
        default:
            return state;
    }
}