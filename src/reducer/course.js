import {
  GET_ALL_COURSE_SUCCESS,
  GET_ALL_COURSE_FAILURE,
  GET_AN_COURSE_SUCCESS,
  GET_AN_COURSE_FAILURE,
} from "../config/ActionTypes";

const init = {
  data: {
    allCourse: [],
    course: {},
  },
  error: {
    allCourse: null,
    course: null,
  },
};

export default function courseReducer(state = init, action) {
  switch (action.type) {
    //READ COURSE
    case GET_ALL_COURSE_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          allCourse: action.data,
        },
      };

    case GET_ALL_COURSE_FAILURE:
      return {
        ...state,
        error: {
          ...state.error,
          allCourse: action.error,
        },
      };

    case GET_AN_COURSE_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          course: action.data,
        },
      };
    case GET_AN_COURSE_FAILURE:
      return {
        ...state,
        error: {
          ...state.error,
          course: action.error,
        },
      };
    default:
      return state;
  }
}
