import Axios from 'axios';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { addClassSuccess, deleteClassSuccess, getAllClassSuccess, getStudentsByClassSuccess, updateClassSuccess, updateScoreStudentSuccess } from '../../action/class';
import { ADD_CLASS, DELETE_CLASS, GET_ALL_CLASS, GET_STUDENTS_BY_CLASS, UPDATE_CLASS, UPDATE_SCORE_STUDENT } from '../../config/ActionTypes';
import { url } from '../../config/API';
function* getAllClass(action){
    const res = yield call(async () => {
        return await Axios.get(url + '/class/get-all');   
    });
    if(res.status == 200){
        yield put(getAllClassSuccess(res.data));
    }
    else
    {
        console.log(res.data);
    }
}

function* getStudentsByClass(action){
    const res = yield call(async () => {
        return await Axios.get(url + '/join/get-students-by-class/' + action.data.id);   
    });
    if(res.status == 200){
        yield put(getStudentsByClassSuccess(res.data));
    }
    else
    {
        console.log(res.data);
    }
}

function* addClass(action){
    const res = yield call(async () => {
        return await Axios.post(url + '/class' , action.data);   
    });
    if(res.status == 200){
        yield put(addClassSuccess(res.data));
    }
    else
    {
        console.log(res.data);
    }
}

function* deleteClass(action){
    const res = yield call(async () => {
        return await Axios.delete(url + '/class/' + action.data.id);   
    });
    if(res.status == 200){
        yield put(deleteClassSuccess(action.data));
    }
    else
    {
        console.log(res.data);
    }
}

function* updateClass(action){
    const res = yield call(async () => {
        return await Axios.put(url + '/class/' + action.data._id);   
    });
    if(res.status == 200){
        yield put(updateClassSuccess(action.data));
    }
    else
    {
        console.log(res.data);
    }
}

function* updateScoreStudent(action){
    const res = yield call(async () => {
        return await Axios.put(url + '/join/update-score/' + `${ action.data.idUser }/${ action.data.idClass }`, action.data);   
    });
    if(res.status == 200){
        yield put(updateScoreStudentSuccess(res.data));
    }
    else
    {
        console.log(res.data);
    }
}

export default function* watchClass(){

    yield all([
        yield takeLatest(GET_ALL_CLASS, getAllClass),
        yield takeLatest(GET_STUDENTS_BY_CLASS, getStudentsByClass),
        yield takeLatest(ADD_CLASS, addClass),
        yield takeLatest(DELETE_CLASS, deleteClass),
        yield takeLatest(UPDATE_CLASS, updateClass),
        yield takeLatest(UPDATE_SCORE_STUDENT, updateScoreStudent)
    ])
}