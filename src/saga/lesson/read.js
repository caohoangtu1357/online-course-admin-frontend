import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { GET_LESSON_BY_ID_COURSE } from "../../config/ActionTypes";
import axios from "axios";

import {
  getLessonByIdCourseFalse,
  getLessonByIdCourseSuccess
} from "../../action/lesson";

function readAsync(idCourse) {
  return axios
    .get(url +"/lesson/get-lesson-by-id-course/"+idCourse)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* read(courseData) {
  try {
    var data = yield call(readAsync,courseData.idCourse);
    yield put(getLessonByIdCourseSuccess(data));
  } catch (err) {
    yield put(getLessonByIdCourseFalse(err));
  }
}

export function* watchGetLessonByIdCourse() {
  yield takeLatest(GET_LESSON_BY_ID_COURSE, read);
}

