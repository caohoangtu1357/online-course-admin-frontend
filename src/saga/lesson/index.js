import {all} from 'redux-saga/effects';
import {watchGetLessonByIdCourse} from "./read";

export default function* rootShoes(){
    yield all([
        watchGetLessonByIdCourse()
    ])
}