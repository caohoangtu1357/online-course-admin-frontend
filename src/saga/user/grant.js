import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { GRANT_ADMIN,GET_ALL_USER } from "../../config/ActionTypes";
import axios from "axios";

function grantAsync(id) {
  return axios
    .get(url + "/user/grantadmin/" + id.id)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* grantadmin(id) {
  yield call(grantAsync, id);
  yield put({type: GET_ALL_USER});
}

export function* watchGrantadmin() {
  yield takeLatest(GRANT_ADMIN, grantadmin);
}
