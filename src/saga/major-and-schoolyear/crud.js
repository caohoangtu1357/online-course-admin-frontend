import Axios from 'axios';
import { all, put, call, takeLatest } from 'redux-saga/effects';
import { getAllMajorSuccess } from '../../action/major';
import { getAllSchoolYearSuccess } from '../../action/schoolYear';
import { GET_ALL_MAJOR, GET_ALL_SCHOOL_YEAR } from '../../config/ActionTypes';
import { url } from '../../config/API';

function* getAllMajor(action){
    const res = yield call(async () => {
        return await Axios.get(url + '/majors/get-all')
    });
    if(res.status == 200){
        yield put(getAllMajorSuccess(res.data));
    }
    else
    {
        console.log(res);
    }
}

function* getAllSchoolYear(action){
    const res = yield call(async () => {
        return await Axios.get(url+ '/school-year/get-all')
    });
    if(res.status == 200){
        yield put(getAllSchoolYearSuccess(res.data));
    }
    else
    {
        console.log(res);
    }
}

export default function* watch(){
    yield all([
         takeLatest(GET_ALL_MAJOR, getAllMajor),
         takeLatest(GET_ALL_SCHOOL_YEAR, getAllSchoolYear)
    ])
}