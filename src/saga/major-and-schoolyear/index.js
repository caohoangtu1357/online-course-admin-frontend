import { all } from 'redux-saga/effects';
import crud from './crud';
export default function* root(){
    yield all([
        crud()
    ]);
}