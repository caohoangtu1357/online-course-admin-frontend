import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { GET_ALL_COURSE, GET_AN_COURSE } from "../../config/ActionTypes";
import axios from "axios";

import {
  getAllCourseFailure,
  getAllCourseSuccess,
  getAnCourseFailure,
  getAnCourseSuccess,
} from "../../action/course";

function readAsync() {
  return axios
    .get(url + "/course/get-all")
    .then((res) => {
      console.log(res.data);
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* read() {
  try {
    var data = yield call(readAsync);
    yield put(getAllCourseSuccess(data));
  } catch (err) {
    yield put(getAllCourseFailure(err));
  }
}

export function* watchGetAllCourse() {
  yield takeLatest(GET_ALL_COURSE, read);
}

function readByIdAsync(idCourse) {
  console.log("id in axios", idCourse);
  return axios
    .get(url + "course/getbyid/" + idCourse)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* readById(action) {
  try {
    var data = yield call(readByIdAsync, action.id);
    yield put(getAnCourseSuccess(data));
  } catch (error) {
    yield put(getAnCourseFailure(error));
  }
}

export function* watchGetAnCourseById() {
  yield takeLatest(GET_AN_COURSE, readById);
}
