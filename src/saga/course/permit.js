import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { PERMIT_COURSE, GET_ALL_COURSE } from "../../config/ActionTypes";
import axios from "axios";

function grantAsync(id) {
  return axios
    .get(url + "/course/permitCourse/" + id.id)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* permitCourse(id) {
  yield call(grantAsync, id);
  yield put({ type: GET_ALL_COURSE });
}

export function* watchPermitCourse() {
  yield takeLatest(PERMIT_COURSE, permitCourse);
}
