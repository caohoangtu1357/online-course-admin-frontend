import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { DELETE_COURSE, GET_ALL_COURSE } from "../../config/ActionTypes";
import axios from "axios";

import {} from "../../action/course";

function deleteAsync(id) {
  return axios
    .delete(url + "/course/delete/" + id.id)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* deleteCourse(id) {
  yield call(deleteAsync, id);
  yield put({ type: GET_ALL_COURSE });
}

export function* watchDeleteCourse() {
  yield takeLatest(DELETE_COURSE, deleteCourse);
}
