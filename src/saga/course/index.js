import { all } from "redux-saga/effects";
import { watchGetAnCourseById, watchGetAllCourse } from "./read";
import { watchDeleteCourse } from "./delete";
import { watchPermitCourse } from "./permit";

export default function* rootShoes() {
  yield all([
    watchGetAnCourseById(),
    watchGetAllCourse(),
    watchDeleteCourse(),
    watchPermitCourse(),
  ]);
}
