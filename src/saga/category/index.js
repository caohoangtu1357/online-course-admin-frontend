import {all} from 'redux-saga/effects';
import {watchGetAnCategoryById,watchGetAllCategory} from "./read";
import {watchAddCategory} from "./create";
import {watchDeleteCategory} from "./delete";
import {watchUpdateCategory} from "./update";

export default function* rootShoes(){
    yield all([
        watchGetAnCategoryById(),
        watchGetAllCategory(),
        watchDeleteCategory(),
        watchAddCategory(),
        watchUpdateCategory()
    ])
}