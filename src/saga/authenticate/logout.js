import {takeLatest,call,put} from 'redux-saga/effects';
import {url} from '../../config/API';
import {LOGOUT,CHECK_IS_LOGGED} from '../../config/ActionTypes';
import axios from 'axios';

function logoutAsync(){
    return axios.get(url+"/logout").then(res=>{
        var data=res.data;
        return Promise.resolve(data);
    }).catch(err=>{
        return Promise.reject(err.response.data);
    });
}

function* logout(actionData){
    yield call(logoutAsync);
    localStorage.removeItem("auth-token");
    yield put({type:CHECK_IS_LOGGED});
}

export function* watchLogout(){
    yield takeLatest(LOGOUT,logout);
}
