import { ADD_TRAINING_PROGRAM, ADD_TRAINING_PROGRAM_COURSE, GET_ALL_TRAINING_PROGRAM, GET_SPECIALIZES_BY_TRAINING_PROGRAM, GET_TRAINING_PROGRAM, GET_TRAINING_PROGRAM_COURSES, REMOVE_TRAINING_PROGRAM_COURSE } from "../../config/ActionTypes";

const { default: Axios } = require("axios");
const { call, put, takeLatest, all } = require("redux-saga/effects");
const { getTrainingProgramsSuccess, getTrainingProgramCoursesSuccess, addTrainingProgramCourseSuccess, getTrainingProgramSuccess, removeTrainingProgramCourseSuccess, addTrainingProgramSuccess, getSpecializeSuccess } = require("../../action/training-program");
const { url } = require("../../config/API");

function* getTrainingPrograms(){
    const res = yield call(async () => {
        return  await Axios.get(url + '/training-program/get-all' );
    });
    if(res.status == 200){
        yield put(getTrainingProgramsSuccess(res.data));
    }
    else
    {
        console.log(res.data);
    }
}   

function* getTrainingProgram(action){
    const id = action.data;
    const res = yield call( async() => {
        return await Axios.get(url + '/training-program/' + id);
    })

    if(res.status == 200){
        console.log(res.data);
        yield put(getTrainingProgramSuccess(res.data));
    }
    else{
        console.log(res);
    }
}

function* getCoursesByTrainingProgram(action){
    const res = yield call(async (idTrainingProgram) => {
        return  await Axios.get(url + '/training-program/get-all-course/' + idTrainingProgram );
    },action.data );
    if(res.status == 200){
        yield put(getTrainingProgramCoursesSuccess(res.data));
    }
    else
    {
        console.log(res.data);
    }
}



function* addTrainingProgramCourse(action){
    const data = action.data;
    const res = yield call(async () => {
        return  await Axios.post(url + '/training-program/add-course' ,data );
    } );
    if(res.status == 200){
        console.log(res.data);
        yield put(addTrainingProgramCourseSuccess(res.data));
    }
    else
    {
        console.log(res.data);
    }
}

function* removeTrainingProgramCourse(action){
    const data = action.data;
    console.log(data);
    const res = yield call(async () => {
      
        return  await Axios.delete(url + '/training-program/remove-course' ,{ data } );
    } );
    if(res.status == 200){
        console.log(res.data);
        yield put(removeTrainingProgramCourseSuccess(data.index));
    }
    else
    {
        console.log(res.data);
    }
}

function* addTrainingProgram(action){
    const res = yield call(async () => {
        return await Axios.post(url + '/training-program', action.data );
    });
    if(res.status == 200){
        yield put(addTrainingProgramSuccess(res.data));
    }
    else
    {
        console.log(res);
    }
}

function* getSpeicializesByTrainingProgram(action){
    const res = yield call(async () => {
        return await Axios.get(url + '/specialize/get-by-training-program/' + action.data.id );
    });
    if(res.status == 200){
        yield put(getSpecializeSuccess(res.data));
    }
    else
    {
        console.log(res);
    }
}
export default function* watchTrainingPrograms(){
    yield all(
        [
            takeLatest(GET_ALL_TRAINING_PROGRAM, getTrainingPrograms),
            takeLatest(GET_TRAINING_PROGRAM, getTrainingProgram),
            takeLatest(GET_TRAINING_PROGRAM_COURSES, getCoursesByTrainingProgram),
            takeLatest(ADD_TRAINING_PROGRAM_COURSE, addTrainingProgramCourse),
            takeLatest(REMOVE_TRAINING_PROGRAM_COURSE, removeTrainingProgramCourse),
            takeLatest(ADD_TRAINING_PROGRAM, addTrainingProgram),
            takeLatest(GET_SPECIALIZES_BY_TRAINING_PROGRAM, getSpeicializesByTrainingProgram)
        ]
    ) 
}