import React, { useEffect } from "react";
import { Redirect, Route, HashRouter as Router } from "react-router-dom";
import { Dashboard, Category, Login, User,Lesson,Course,Revenue, TrainingProgram, TrainingProgramDetail,ManageCourseTrainingProgram } from "./screen/index.js";
import { connect } from "react-redux";
import { CHECK_IS_LOGGED } from "./config/ActionTypes";
import ClassList from "./screen/class/index.js";
import ClassDetail from "./screen/ClassDetail/index.js";

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.checkIsLogged();
  }

  render() {
    return (
      <Router>
        <div>
          <Route
            exact
            path="/"
            render={() => {
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <Dashboard />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />
          <Route
            exact
            path="/category"
            render={() => {
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <Category />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />
          <Route
            exact
            path="/user"
            render={() => {
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <User />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />

          <Route 
            exact
            path="/lesson/:idCourse"
            render={()=>{
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <Lesson />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />

          <Route 
            exact
            path="/course"
            render={()=>{
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <Course />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />

          <Route
            exact
            path="/revenue"
            render={()=>{
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <Revenue />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />
          <Route
            exact
            path="/training-program"
            render={()=>{
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <TrainingProgram />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />
           <Route
            exact
            path="/training-program/:id"
            render={()=>{
              if (this.props.user) {
                if(this.props.user.role=="admin"){
                  return <TrainingProgramDetail />;
                }
                return <Login />;
              } else {
                return <Login />;
              }
            }}
          />
          <Route
          exact
          path="/class"
          render={()=>{
            if (this.props.user) {
              if(this.props.user.role=="admin"){
                return <ClassList />;
              }
              return <Login />;
            } else {
              return <Login />;
            }
          }}
          />
          <Route
          exact
          path="/class/:id"
          render={()=>{
            if (this.props.user) {
              if(this.props.user.role=="admin"){
                return  <ClassDetail />;
              }
              return <Login />;
            } else {
              return <Login />;
            }
          }}
          />
            <Route
                exact
                path="/manage-course-training-program"
                render={()=>{
                    if (this.props.user) {
                        if(this.props.user.role=="admin"){
                            return  <ManageCourseTrainingProgram />;
                        }
                        return <Login />;
                    } else {
                        return <Login />;
                    }
                }}
            />

          <Route exact path="/login" component={Login} />
        </div>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.authenticate.data,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    checkIsLogged: () => dispatch({ type: CHECK_IS_LOGGED }),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
