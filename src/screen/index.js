import Dashboard from "./dashboard/Dashboard.jsx";
import Category from "./category/category.jsx";
import Login from "./Login/Login.jsx";
import User from "./User/User.jsx";
import Course from "./course/course.jsx";
import Lesson from "./lesson/Lesson.jsx";
import Revenue from "./revenue/Revenue.jsx";
import TrainingProgram from './training-program';
import TrainingProgramDetail from './training-program-detail';
import ManageCourseTrainingProgram from './manage-course-training-program';

export { 
    Dashboard, 
    Category, 
    Login, User, 
    Lesson,
    Course,
    Revenue, 
    TrainingProgram,
    TrainingProgramDetail,
    ManageCourseTrainingProgram
};
