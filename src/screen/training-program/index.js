import { TextField } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import React, { Fragment, useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Menu from '../../component/menu/Menu';
import ModalComponent from '../../component/modal';
import SideBar from '../../component/SideBar/SideBar';
import { ADD_TRAINING_PROGRAM, GET_ALL_MAJOR, GET_ALL_SCHOOL_YEAR, GET_ALL_TRAINING_PROGRAM } from '../../config/ActionTypes';
import './style.scss';
const TrainingProgram = props => {

    const [ openModal, setOpenModal ] = useState(false);
    const [ trainingProgram , setTrainingProgram ] = useState({
        name: '',
        idMajor : '',
        idSchoolYear : ''
    });

    useEffect(()=>{
        props.dispatch({ type: GET_ALL_MAJOR });
        props.dispatch({ type: GET_ALL_TRAINING_PROGRAM });
        props.dispatch({ type: GET_ALL_SCHOOL_YEAR });
       
    }, []);
   
    function handleAddTrainingProgram(){
        console.log(trainingProgram);
        if(!trainingProgram.name || !trainingProgram.idMajor || !trainingProgram.idSchoolYear){
            alert('Bạn phải nhập đầy đủ thông tin');
            return;
        }
        else
        {
            props.dispatch({
                type: ADD_TRAINING_PROGRAM,
                data: trainingProgram
            });
            setTrainingProgram({
                name: '',
                idMajor: trainingProgram.idMajor,
                idSchoolYear: trainingProgram.idSchoolYear,
            })
        }
    }

    function renderTrainingPrograms(){
        if(props.trainingProgram.list.length > 0){
            const dataGroup = props.trainingProgram.list.reduce((r,a) => {
                r[a.idMajor._id] = [ ...r[a.idMajor._id] || [], a];
                return r;
            }, []);

           console.log(dataGroup);
           
            return  props.major.list.map((item,index) => 
                <tr key = { 'major' + index } className = 'row-item'>
                    <td>{ index + 1 }</td>
                    <td>{ item.name }</td>
                    <td>
                        {
                            dataGroup[item._id] &&
                            dataGroup[item._id]
                            .sort((a,b) => a.name.substr(a.name.length -2) - b.name.substr(b.name.length-2))
                            .map((a) => 
                            <Link key = { a._id } to = { '/training-program/' + a._id } className = 'item'>
                                { a.name }
                            </Link>
                            )
                        }
                        
                    </td>
                </tr>)
        }
        else
        {
            return <div>Chưa có chương trình đào tạo </div>
        }
       
    }

    return (
        <Container fluid>
        <Row className="wrapper">
          <Col xs={2} className="col">
            <SideBar/>
          </Col>
          <Col xs={10} className="col content-wrapper">
            <Menu/>
            <div className="main-content training-program-container">
                <div className = 'btn-add'>
                    <button onClick = { () => setOpenModal(true) }  className = 'btn btn-success'>Thêm chương trình đào tạo</button>
                    {
                        <ModalComponent 
                        open = { openModal }
                        setOpen = { setOpenModal }
                        title = 'Thêm chương trình đào tạo' 
                        renderComponents = 
                        { 
                            <Fragment>
                                <div className = 'row-input'>
                                    <label>Chương trình đào tạo</label>
                                    <TextField value = { trainingProgram.name } onChange = {(e) => setTrainingProgram({ ...trainingProgram, name: e.target.value })} variant = 'outlined' fullWidth size = 'small' placeholder = 'name...' />
                                </div>
                                 <div className = 'row-input'>
                                    <label>Ngành học</label>
                                    <Autocomplete
                                    id="combo-box-demo"
                                    options={ props.major.list || [] }
                                    getOptionLabel={(option) => option.name }
                                    onChange = {(event, value) => setTrainingProgram({ ...trainingProgram, idMajor : value ? value._id : '' }) }
                                    fullWidth
                                    renderInput={(params) => 
                                        <TextField 
                                        fullWidth
                                        placeholder = 'Tên ngành'
                                        {...params} 
                                        variant="outlined" />
                                        
                                        }
                                    />
                                 </div>
                                <div className = 'row-input'>
                                    <label>Năm bắt đầu</label>
                                    <Autocomplete
                                    id="combo-box-demo"
                                   
                                    options={ props.schoolYear.list }
                                    getOptionLabel={(option) => '' + option.year}
                                    onChange = {(event, value) => setTrainingProgram({ ...trainingProgram, idSchoolYear: value ? value._id : '' }) }
                                    fullWidth
                                    renderInput={(params) => 
                                        <TextField 
                                        fullWidth
                                        placeholder = 'Năm bắt đầu'
                                        {...params} 
                                        variant="outlined" />
                                        
                                        }
                                    />
                                </div>
                               <div className = 'row-input'>
                                   <button onClick = { handleAddTrainingProgram } className = 'btn btn-success'>Thêm chương trình đào tạo</button>
                                </div> 
                            </Fragment>
                           
                        } />
                    }
                </div>
                <div className = 'list-training-program'>
                    <h3>Danh sách chương trình đào tạo</h3>
                    <table>
                        <tr>
                            <th>Stt</th>
                            <th>Ngành</th>
                            <th>Chương trình đào tạo</th>
                            
                        </tr>
                        {
                           /*  props.trainingProgram.list.sort((a,b) => a.name.substr(a.name.length -2) - b.name.substr(b.name.length-2) ).map((item, index) => 
                                <tr key = { item._id } className = 'row-item'>
                                    <td>{ index + 1 }</td>
                                    <td>
                                        <Link to = { '/training-program/' + item._id } className = 'item'>
                                           { item.name }
                                        </Link>
                                    </td>
                                </tr>
                                ) */
                                renderTrainingPrograms()
                        }
                    </table>
                    
                   
                </div>
            </div>
          </Col>
        </Row>
      </Container>
    );
};


const mapStateToProps = state => ({
    trainingProgram: state.trainingProgram,
    major : state.major,
    schoolYear: state.schoolYear
});

const mapDispatchToProps = dispatch => ({
    dispatch
});

export default connect(mapStateToProps,mapDispatchToProps)(TrainingProgram);