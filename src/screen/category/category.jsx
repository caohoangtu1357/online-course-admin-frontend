import React, { useState, useEffect } from "react";
import {
  Button,
  Container,
  Row,
  Col,
  Table,
  Form,
  Alert,
} from "react-bootstrap";
import "./styleCategory.css";
import SideBar from "../../component/SideBar/SideBar.jsx";
import Menu from "../../component/menu/Menu.jsx";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import {
  GET_ALL_CATEGORY,
  ADD_CATEGORY,
  DELETE_CATEGORY,
  UPDATE_CATEGORY,
} from "../../config/ActionTypes";
import { url } from "../../config/API";
import CustomAlert from "../../component/alert/alert.jsx";

function Category(props) {
  const [display, setDisplay] = useState("hidden");
  const [opacity, setOpacity] = useState(0);
  const [categoryName, setCategoryName] = useState("");
  const [categoryImage, setCategoryImage] = useState("");
  const [displayUpdate, setDisplayUpdate] = useState("hidden");
  const [opacityUpdate, setOpacityUpdate] = useState(0);
  const [idUpdate, setIdUpdate] = useState("");

  useEffect(() => {
    props.getAllCategory();
  }, []);

  function displayModel() {
    setDisplay("visible");
    setOpacity(1);
  }

  function closeModel() {
    setDisplay("hidden");
    setOpacity(0);
  }

  function addCategoryBtn() {
    props.addCategory({ name: categoryName, image: categoryImage });
    if (props.newCategory != null) {
      setCategoryName("");
      closeModel();
    }
  }

  function deleteBtn(e) {
    props.deleteCategory(e.target.getAttribute("idcategory"));
  }

  function updateBtn(e) {
    console.log(e.target.getAttribute("idcategory"));
    setIdUpdate(e.target.getAttribute("idcategory"));
    setDisplayUpdate("visible");
    setOpacityUpdate(1);
  }

  function closeModelUpdate() {
    setDisplayUpdate("hidden");
    setOpacityUpdate(0);
  }

  function updateBtnModel() {
    props.updateCategory({
      id: idUpdate,
      name: categoryName,
      image: categoryImage,
    });
    closeModelUpdate();
  }

  return (
    <Container fluid>
      <Row className="wrapper">
        <Col xs={2} className="col">
          <SideBar />
        </Col>
        <Col xs={10} className="col content-wrapper">
          <Menu />
          {/* Model Add new category */}
          <div
            className="model-add-category"
            style={{ visibility: display, opacity: opacity }}
          >
            <span onClick={closeModel}>
              <FontAwesomeIcon
                style={{ float: "right", cursor: "pointer" }}
                size="lg"
                className="icon-sidebar"
                icon={faTimes}
              />
            </span>
            <Form.Group controlId="exampleForm.ControlInput1">
              <Form.Label>Tên danh mục:</Form.Label>
              <Form.Control
                type="text"
                value={categoryName}
                onChange={(e) => {
                  setCategoryName(e.target.value);
                }}
                placeholder="Tên danh mục"
              />
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlInput1">
              <Form.Label>Hình ảnh:</Form.Label>
              <Form.Control
                type="file"
                onChange={(e) => {
                  setCategoryImage(e.target.files[0]);
                }}
              />
            </Form.Group>
            <Button
              style={{ float: "right" }}
              variant="primary"
              onClick={addCategoryBtn}
            >
              Thêm mới
            </Button>
          </div>

          <div
            className="model-add-category"
            style={{ visibility: displayUpdate, opacity: opacityUpdate }}
          >
            <span onClick={closeModelUpdate}>
              <FontAwesomeIcon
                style={{ float: "right", cursor: "pointer" }}
                size="lg"
                className="icon-sidebar"
                icon={faTimes}
              />
            </span>
            <Form.Group controlId="exampleForm.ControlInput1">
              <Form.Label>Tên danh mục:</Form.Label>
              <Form.Control
                type="text"
                value={categoryName}
                onChange={(e) => {
                  setCategoryName(e.target.value);
                }}
                placeholder="Tên danh mục"
              />
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlInput1">
              <Form.Label>Hình ảnh:</Form.Label>
              <Form.Control
                type="file"
                onChange={(e) => {
                  setCategoryImage(e.target.files[0]);
                }}
              />
            </Form.Group>
            <Button
              style={{ float: "right" }}
              variant="primary"
              onClick={updateBtnModel}
            >
              Cập nhật
            </Button>
          </div>

          <Row className="main-content">

            <div style={{width:"100%"}}>
              <CustomAlert variant="danger" />
            </div>
            <div>
              <h2 className="heading">Quản lý danh mục</h2>
            </div>
            <div>
              <Button
                className="right-btn"
                variant="success"
                onClick={displayModel}
              >
                Thêm mới danh mục
              </Button>
            </div>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <td>STT</td>
                  <td>Hình ảnh</td>
                  <td>Tên danh mục</td>
                  <td>Tùy Chọn</td>
                </tr>
              </thead>
              <tbody>
                {props.categories
                  ? props.categories.map((category, index) => {
                      return (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td>
                            <img
                              className="img"
                              src={url + "/upload/category/" + category.image}
                              alt=""
                            />
                          </td>
                          <td>{category.name}</td>
                          <td>
                            <Button
                              className="btn-option"
                              idcategory={category._id}
                              onClick={(e) => {if(window.confirm('Bạn có chắc mốn xóa danh mục này không?')){deleteBtn(e)};}}
                              
                              variant="danger"
                            >
                              Xóa
                            </Button>
                            <Button
                              className="btn-option"
                              idcategory={category._id}
                              variant="warning"
                              onClick={updateBtn}
                            >
                              Sửa
                            </Button>
                          </td>
                        </tr>
                      );
                    })
                  : ""}
              </tbody>
            </Table>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

function mapStateToProps(state) {
  console.log("category",state.category);
  return {
    categories: state.category.data.allCategory,
    errorAddCategory: state.category.error.category,
    newCategory: state.category.data.category
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getAllCategory: () => dispatch({ type: GET_ALL_CATEGORY }),
    addCategory: (category) => dispatch({ type: ADD_CATEGORY, category }),
    deleteCategory: (id) => dispatch({ type: DELETE_CATEGORY, id }),
    updateCategory: (category) => dispatch({ type: UPDATE_CATEGORY, category }),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Category);
