import React, { useState, useEffect } from "react";
import {
  Button,
  Container,
  Row,
  Col,
  Table,
  Form,
  Alert,
} from "react-bootstrap";
import "./styleCourse.css";
import {Link} from "react-router-dom";
import SideBar from "../../component/SideBar/SideBar.jsx";
import Menu from "../../component/menu/Menu.jsx";
import { connect } from "react-redux";
import {
  GET_ALL_COURSE,
  DELETE_COURSE,
  PERMIT_COURSE,
} from "../../config/ActionTypes";
import { url } from "../../config/API";

function Course(props) {
  useEffect(() => {
    props.getAllCourse();
  }, []);

  function deleteBtn(e) {
    props.deleteCourse(e.target.getAttribute("idcourse"));
  }

  function permitCourse(e) {
    props.permitCourse(e.target.getAttribute("idcourse"));
  }

  console.log(props.courses);
  return (
    <Container fluid>
      <Row className="wrapper">
        <Col xs={2} className="col">
          <SideBar />
        </Col>
        <Col xs={10} className="col content-wrapper">
          <Menu />

          <Row className="main-content">
            <div>
              <h2 className="heading">Quản lý Course</h2>
            </div>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <td>STT</td>
                  <td>Tên khóa học</td>
                  <td>Hình ảnh</td>
                  <td>Lĩnh vực</td>
                  <td>Người tạo</td>
                  <td>Mục tiêu</td>
                  <td>Mô tả</td>
                  <td>Thao tác</td>
                </tr>
              </thead>
              <tbody>
                {props.courses?props.courses.map((course, index) => {
                      return (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td>{course.name}</td>
                          <td>
                            <img
                              className="img"
                              src={url + "/upload/course_image/" + course.image}
                              alt={course.name}
                            />
                          </td>

                          {/*<td>{course.category.name}</td>*/}
                          <td>{course.idUser.name}</td>
                          <td>{course.goal.length>200?course.goal.substring(0,200)+"...":course.goal}</td>
                          <td>{course.description.length>200?course.description.substring(0,200)+"...":course.description}</td>
                          <td>
                            <Button
                              className="btn-option"
                              idcourse={course._id}
                              onClick={(e) => {if(window.confirm('Bạn có chắc mốn xóa khóa học này không?')){deleteBtn(e)};}}
                              variant="danger"
                            >
                              Xóa
                            </Button>
                            {course.is_checked ? (
                              <Button
                                disabled
                                className="btn-option"
                                idcourse={course._id}
                                variant="success"
                              >
                                Đã Duyệt
                              </Button>
                            ) : (
                              <Button
                                className="btn-option"
                                idcourse={course._id}
                                variant="success"
                                onClick={permitCourse}
                              >
                                Duyệt
                              </Button>
                            )}
                            <Link to={"/lesson/"+course._id}>
                              <Button
                                className="btn-option"
                                variant="info"
                              >
                                Xem bài học
                              </Button>
                            </Link>

                          </td>
                        </tr>
                      );
                    })
                  : ""}
              </tbody>
            </Table>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

function mapStateToProps(state) {
  return {
    courses: state.course.data.allCourse,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getAllCourse: () => dispatch({ type: GET_ALL_COURSE }),
    deleteCourse: (id) => dispatch({ type: DELETE_COURSE, id }),
    permitCourse: (id) => dispatch({ type: PERMIT_COURSE, id }),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Course);
