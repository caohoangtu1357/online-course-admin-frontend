import { TextField } from '@material-ui/core';
import React, { Fragment, useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';
import Menu from '../../component/menu/Menu';
import SideBar from '../../component/SideBar/SideBar';
import { GET_STUDENTS_BY_CLASS, UPDATE_SCORE_STUDENT } from '../../config/ActionTypes';

const ClassDetail = ( props ) => {

    const { id } = useParams();
    const classInfo = props.classes.filter(item => item._id == id ).pop();
    const [ dataScore , setDataScore ] = useState({});
    useEffect(() => {
        props.dispatch({ type: GET_STUDENTS_BY_CLASS, 
            data : { id } 
        })
    }, [ id ]);

    function updateScoreStudent(e){
        if(e.keyCode == 13){
            props.dispatch({
                type: UPDATE_SCORE_STUDENT,
                data : dataScore
            });
            setDataScore({});
        }
      
    }
    return (
    <Container fluid>
        <Row className="wrapper">
          <Col xs={2} className="col">
            <SideBar/>
          </Col>
          <Col xs={10} className="col content-wrapper">
            <Menu />
            <div className="main-content class-container">
                <div className = 'info block'>
                    <h4 className = 'title'>Thông tin về lớp học</h4>
                   {
                       classInfo &&
                       <Fragment>
                            <h5><strong>Tên: </strong>{ classInfo.name }</h5>
                            <h5><strong>Khoa: </strong>{ classInfo.idCategory?.name }</h5>
                            <h5><strong>Giảng viên: </strong>{ classInfo.idTeacher?.name }</h5>
                            <h5><strong>Sỹ số:  </strong>{ classInfo.numberStudent }</h5>
                            <h5><strong>Kỳ học: </strong>{ classInfo.term }</h5>
                            <h5><strong>Năm: </strong>{ classInfo.year }</h5>
                       </Fragment>
                   }
                </div>
                <div className = 'class-info block'>
                    <h4 className = 'title'>Danh sách sinh viên </h4>
                    <table>
                        <tr>
                            <th>Stt</th>
                            <th>Tên</th>
                            <th>MSSV</th>
                            <th>Điểm TB</th>
                            <th>Action</th>
                        </tr>
                      
                        {
                            props.students.sort((a,b) => b.score - a.score).map((item, index )=> 
                                <tr key = { item._id }>
                                    <td> { index + 1 } </td>
                                    <td> { item.idUser && item.idUser.name } </td>
                                    <td> { item.idUser?.code } </td>
                                    <td> { 
                                        dataScore.idUser == item.idUser._id ?
                                        <TextField 
                                        fullWidth
                                        min = '0'
                                        max = '10'
                                        type = 'number'
                                        onKeyUp = { updateScoreStudent }
                                        value = { dataScore.score } 
                                        onChange = { (e) => setDataScore({ ...dataScore, score: e.target.value }) } /> :
                                        item.score 
                                        } 
                                    </td>
                                    <td>
                                        <button onClick = { () => setDataScore({ idUser : item.idUser._id, idClass : id, score: item.score })} className = 'btn btn-info'>Sửa điểm</button> 
                                        <button className = 'btn btn-danger'>Xóa</button> 
                                    </td>
                                    
                                </tr>
                                )
                        }
                        
                    </table>
                </div>
            </div>
          </Col>
        </Row>
      </Container>
    );
};

const mapStateToProps = state => ({
    classes : state.class.list,
    students : state.class.students
})

const mapDispatchToProps = dispatch => ({
    dispatch
})
export default connect(mapStateToProps, mapDispatchToProps)(ClassDetail); 