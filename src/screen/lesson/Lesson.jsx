import React, { useState, useEffect } from "react";
import { useParams} from "react-router";
import {
  Button,
  Container,
  Row,
  Col,
  Table,
  Form,
  Alert,
} from "react-bootstrap";
import "./lesson.css";
import SideBar from "../../component/SideBar/SideBar.jsx";
import Menu from "../../component/menu/Menu.jsx";
import { faFilePdf,faFileWord,faFileExcel,faPlus,faMinus,faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import {
  GET_LESSON_BY_ID_COURSE
} from "../../config/ActionTypes";
import { url } from "../../config/API";

function LessonDetail(props){
    return(
        <div className="lesson-content">
            <div className="head-type-content">
                <h5>Video Bài Giảng:</h5>
            </div>
            <div className="video-show">
                <video controls>
                    <source src={url+"/upload/lesson/"+props.lessonNeedShow.video} type="video/mp4"/>
                </video>
            </div>
            <div className="head-type-content">
                <h5>Danh sách file văn bản:</h5>
            </div>
            <div>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                        <td>Tên File</td>
                        <td>Loại File</td>
                        <td>Xem chi Tiết</td>
                        </tr>
                    </thead>
                    <tbody>
                        {props.lessonNeedShow.doc.map((itm,index)=>{
                            return(
                                <tr>
                                    <td style={{textAlign:"left"}}>{itm}</td>
                                    <td>                        
                                        <span>
                                            <FontAwesomeIcon style={{color:"red"}} icon={itm.split(".")[1]=="doc"?faFileWord:faFilePdf} size="lg" style={{marginRight:12, color:itm.split(".")[1]=="doc"?"blue":"red"}} />{itm.split(".")[1]}
                                        </span>
                                    </td>
                                    <td>
                                        <a href={url+"/upload/lesson/"+itm}>Download</a>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </Table>
            </div>
            <div className="head-type-content">
                <h5>Danh sách câu hỏi trắc nghiệm:</h5>
            </div>
            <div className="list-multiple-choice">
                {props.lessonNeedShow.multipleChoices.map((itm,index)=>{
                    return(<div>
                        <div className="question">
                            {itm.question} 
                        </div>
                        <div className="multiple-choice-option">
                            <div>Câu A. {itm.A}</div>
                            <div>Câu B. {itm.B}</div>
                            <div>Câu C. {itm.C}</div>
                            <div>Câu D. {itm.D}</div>
                        </div>
                        <div className="answer">
                            -&gt; Câu trả lời đúng: Câu {itm.answer}
                        </div>
                    </div>
                    )
                })}                
            </div>
        </div>       
    )
}


function Lesson(props) {
    const [display, setDisplay] = useState("none");
    const [displayMainContent,setDisplayMainContent]=useState("block");
    const [lessonNeedShow,setLessonNeedShow]=useState(null);

    function displayModel(e) {
        setLessonNeedShow(e);
        console.log(e);
        setDisplay("block");
        setDisplayMainContent("none");
    }
    
    function closeModel() {
        setDisplay("none");
        setDisplayMainContent("block");
    }

    let { idCourse } = useParams();
    useEffect(() => {
        props.getLessonByIdCourse(idCourse);
    }, []);



    console.log("sadasdasdasdasdad",props.lesson);
    

    return (
        <Container fluid>
        <Row className="wrapper">
            <Col xs={2} className="col">
            <SideBar />
            </Col>
            <Col xs={10} className="col content-wrapper">
            <Menu />

            {/* Model */}
            <div className="model-lesson-detail" style={{ display: display}}>
                <span onClick={closeModel}>
                <FontAwesomeIcon
                    style={{ float: "right", cursor: "pointer" }}
                    size="lg"
                    className="icon-sidebar"
                    icon={faTimes}
                />
                </span>
                {lessonNeedShow?<LessonDetail lessonNeedShow={lessonNeedShow}/>:""}
                
            </div>
            <Row className="main-content" style={{display:displayMainContent}}>
                <div>
                <h2 className="heading">Chi Tiết Khóa Học</h2>
                </div>
                <div style={{width:"100%"}}  className="lesson">
                    {props.lesson?props.lesson.map((itm,index)=>{
                        return(
                            <div key={index} className="lesson-name">
                                <h4 onClick={()=>displayModel(itm)}>
                                    <span><FontAwesomeIcon className="fa-plus" icon={faPlus} size="sm" /></span>
                                    {itm.title}
                                </h4>
                            </div>
                        )
                    }):""}
                </div>
            </Row>
            </Col>
        </Row>
        </Container>
    );
}

function mapStateToProps(state) {
  return {
    lesson:state.lesson.data.lessonByIdCourse
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getLessonByIdCourse:(idCourse)=>dispatch({type:GET_LESSON_BY_ID_COURSE,idCourse})
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Lesson);
