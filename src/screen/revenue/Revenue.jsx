import React from 'react';
import {Button,Container,Row,Col} from 'react-bootstrap';
import './styleRevenue.css';
import SideBar from '../../component/SideBar/SideBar.jsx';
import Menu from '../../component/menu/Menu.jsx';
import LineChart from '../../component/chart/LineChart.jsx';
import PieChart from '../../component/chart/PieChart.jsx';

export default function DashBoard(props){
  return(
    <Container fluid>
      <Row className="wrapper">
        <Col xs={2} className="col">
          <SideBar/>
        </Col>
        <Col xs={10} className="col content-wrapper">
          <Menu/>
          <Row className="main-content">
            <Col xs={8}>
              <LineChart/>
            </Col>
            <Col xs={4}>
              <PieChart/>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  )
}