import React, { useState, useEffect } from "react";
import {
  Button,
  Container,
  Row,
  Col,
  Table,
  Form,
  Alert,
} from "react-bootstrap";
import "./styleUser.css";
import SideBar from "../../component/SideBar/SideBar.jsx";
import Menu from "../../component/menu/Menu.jsx";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import {
  GET_ALL_USER,
  DELETE_USER,
  GRANT_ADMIN,
} from "../../config/ActionTypes";
import { url } from "../../config/API";
import CustomAlert from "../../component/alert/alert.jsx";


function User(props) {
  useEffect(() => {
    props.getAllUser();
  }, []);

  function deleteBtn(e) {
    props.deleteUser(e.target.getAttribute("iduser"));
  }

  function grantadmin(e) {
    props.grantadmin(e.target.getAttribute("iduser"));
  }

  return (
    <Container fluid>
      <Row className="wrapper">
        <Col xs={2} className="col">
          <SideBar />
        </Col>
        <Col xs={10} className="col content-wrapper">
          <Menu />

          <Row className="main-content">
            <div style={{width:"100%"}}>
              <CustomAlert variant="danger" />
            </div>
            <div>
              <h2 className="heading">Quản lý User</h2>
            </div>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <td>STT</td>
                  <td>Hình ảnh</td>
                  <td>Tên hiển thị</td>
                  <td>Email</td>
                  <td>SĐT</td>
                  <td>Quyền</td>
                  <td>Thao tác</td>
                </tr>
              </thead>
              <tbody>
                {props.users
                  ? props.users.map((user, index) => {
                      return (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td>
                            <img
                              className="img"
                              src={url + "/upload/user_image/" + user.image}
                              alt={user.name}
                            />
                          </td>
                          <td>{user.name}</td>
                          <td>{user.email}</td>
                          <td>{user.phone}</td>
                          <td>{user.role}</td>
                      
                          <td>
                            <Button
                              className="btn-option"
                              iduser={user._id}
                              onClick={(e) => {if(window.confirm('Bạn có chắc mốn xóa khóa học này không?')){deleteBtn(e)};}}
                              variant="danger"
                            >
                              Xóa
                            </Button>
                            <Button
                              className="btn-option"
                              iduser={user._id}
                              variant="warning"
                              onClick={grantadmin}
                            >
                              Nâng quyền
                            </Button>
                          </td>
                        </tr>
                      );
                    })
                  : ""}
              </tbody>
            </Table>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

function mapStateToProps(state) {
  return {
    users: state.user.data.allUser,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getAllUser: () => dispatch({ type: GET_ALL_USER }),
    deleteUser: (id) => dispatch({ type: DELETE_USER, id }),
    grantadmin: (id) => dispatch({ type: GRANT_ADMIN, id }),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(User);
