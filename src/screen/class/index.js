import React, { Fragment, useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import SideBar from '../../component/SideBar/SideBar';
import Menu from '../../component/menu/Menu';
import { connect } from 'react-redux';
import { ADD_CLASS, DELETE_CLASS, GET_ALL_CATEGORY, GET_ALL_CLASS, GET_ALL_COURSE, GET_ALL_TEACHER, UPDATE_CLASS } from '../../config/ActionTypes';

import './style.scss';
import { Link } from 'react-router-dom';
import ModalComponent from '../../component/modal';
import { TextField } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';

const ClassList = ( props ) => {
    const [ openModal, setOpenModal ] = useState(false);
    const [ modalProps, setModalProps ] = useState({
        title : 'Thêm lớp học',
        type : 'add',
    });
    const [ classData , setClassData ] = useState({
        name : '',
        idCourse : '',
        idTeacher : '',
        idCategory: '',
        term : 1,
        year : new Date().getFullYear()
    });
    const [ classes , setClasses ] = useState(props.classes);

    useEffect(() => {

        setClasses(props.classes);
        if(props.courses.length == 0){
            props.dispatch({ type : GET_ALL_COURSE });
        }
        if(props.classes.length == 0){
            props.dispatch({
                type : GET_ALL_CLASS
            });
        }
       

        props.dispatch({
            type: GET_ALL_TEACHER
        });
        
        if(props.categories.length == 0){
            props.dispatch({
                type: GET_ALL_CATEGORY
            })
        }
        
    }, [ props.classes ]);

    function filterByCategory(e){
        const idCategory = e.target.value;
        if(idCategory == 'all'){
            setClasses(props.classes);
        }
        else
        {
            setClasses(props.classes.filter(item => item.idCategory?._id == idCategory));
        }
    }

    function handleClickModal(){
       if(modalProps.type == 'add')
       {
            if(!classData.idCategory || !classData.idCourse || !classData.idTeacher){
                window.alert('Bạn cần nhập đầy đủ thông tin');
            }
            else
            {
                props.dispatch({
                    type: ADD_CLASS,
                    data : classData
                });
                setOpenModal(false);
            }
       }
       else
       {
            props.dispatch({
                type: UPDATE_CLASS,
                data: classData
            });
            setOpenModal(false);
       }
        
    }

    function addClass(){
        setModalProps({ title: 'Thêm lớp học', type: 'add' });
        setClassData({
            name : '',
            idCourse : '',
            idTeacher : '',
            idCategory: '',
            term : 1,
            year : new Date().getFullYear()
        });
        setOpenModal(true);
    }

    function updateClass(data){
        setModalProps({ title: 'Cập nhật thông tin lớp học', type: 'update' });
        setClassData(data);
        setOpenModal(true);
    }

    function deleteClass(id){
        const confirm = window.confirm('Bạn có chắc chán muốn xóa lớp này?');
        if(confirm){
            props.dispatch({ type: DELETE_CLASS , data : { id }});
        }
    }


    return (
        <Container fluid>
        <Row className="wrapper">
          <Col xs={2} className="col">
            <SideBar/>
          </Col>
          <Col xs={10} className="col content-wrapper">
            <Menu />
            <div className="main-content class-container">
            <div className = 'btn-add'>
                    <button onClick = { addClass } className = 'btn btn-success'>Thêm lớp học</button>
                    {
                        <ModalComponent 
                            open = { openModal }
                            setOpen = { setOpenModal }
                            title = { modalProps.title }
                            renderComponents = {
                                <Fragment>
                                    <div className = 'row-input'>
                                        <label>Tên lớp</label>
                                        <TextField value = { classData.name } onChange = { (e) => setClassData( { ...classData, name : e.target.value } )} variant = 'outlined' fullWidth size = 'small' placeholder = 'Tên lớp...' />
                                    </div>
                                    <div className = 'row-input'>
                                        <label>Môn học</label>
                                        <Autocomplete
                                        id="combo-box-demo"
                                        options={ props.courses }
                                        getOptionLabel={(option) => '' + option.name}
                                        onChange = {(event, value) => setClassData({ ...classData, idCourse: value ? value._id : '' }) }
                                        fullWidth
                                        renderInput={(params) => 
                                            <TextField 
                                            fullWidth
                                            placeholder = 'Môn học'
                                            {...params} 
                                            variant="outlined" />
                                            
                                            }
                                        />
                                    </div>
                                    <div className = 'row-input'>
                                        <label>Giảng viên</label>
                                        <Autocomplete
                                        id="combo-box-demo"
                                        options={ props.teachers }
                                        getOptionLabel={(option) => '' + option.name}
                                        onChange = {(event, value) => setClassData({ ...classData, idTeacher: value ? value._id : '' }) }
                                        fullWidth
                                        renderInput={(params) => 
                                            <TextField 
                                            fullWidth
                                            placeholder = 'Giảng viên'
                                            {...params} 
                                            variant="outlined" />
                                            
                                            }
                                        />
                                    </div>
                                    <div className = 'row-input'>
                                        <label>Khoa</label>
                                        <Autocomplete
                                        id="combo-box-demo"
                                        options={ props.categories }
                                        getOptionLabel={(option) => '' + option.name}
                                        onChange = {(event, value) => setClassData({ ...classData, idCategory: value ? value._id : '' }) }
                                        fullWidth
                                        renderInput={(params) => 
                                            <TextField 
                                            fullWidth
                                            placeholder = 'Khoa'
                                            {...params} 
                                            variant="outlined" />
                                            }
                                        />
                                    </div>
                                    <div className = 'row-input'>
                                        <label>Kỳ học</label>
                                        <TextField type = 'number' value = { classData.term } onChange = { (e) => setClassData({ ...classData, term : e.target.value}) } variant = 'outlined' fullWidth size = 'small' placeholder = 'kỳ...' />
                                    </div>
                                    <div className = 'row-input'>
                                        <label>Năm</label>
                                        <TextField type = 'number' value = { classData.year } onChange = { (e) => setClassData({ ...classData, year : e.target.value}) } variant = 'outlined' fullWidth size = 'small' placeholder = 'Năm...' />
                                    </div>
                                    <div className = 'row-input'>
                                        <button onClick = {  handleClickModal } className = 'btn btn-success'>Thêm lớp học</button>
                                    </div> 
                                </Fragment>
                            }
                        />
                    }
                </div>
               
                <div className = 'class-filter'>
                    <h3>Danh sách các lớp</h3>
                    <div className = 'category-filter'>
                        <label>Khoa</label>
                        <select onChange = { filterByCategory } >
                            <option value = 'all'>All</option>
                            {
                                props.categories.map(item => 
                                <option key = { 'filter-' + item._id } value = { item._id }>{ item.name }</option>
                                    )
                            }
                        </select>
                    </div>
                </div>
                <table>
                    <tr>
                        <th>STT</th>
                        <th>Tên</th>
                        <th>Môn học</th>
                        <th>Khoa</th>
                        <th>Giảng viên</th>
                        <th>Sỹ số</th>
                        <th>Năm</th>
                        <th>Kỳ học</th>
                        <th></th>
                    </tr>
                    {
                        classes.map((item,index) => 
                        <tr className = 'row-item' key = { item._id }>
                            <td> { index + 1 } </td>
                            <td><Link  to = { '/class/' + item._id } > { item.name } </Link></td>
                            <td> { item.idCourse?.name } </td>
                            <td> { item.idCategory?.name } </td>
                            <td> { item.idTeacher?.name } </td>
                            <td> { item.numberStudent } </td>
                            <td> { item.year } </td>
                            <td> { item.term } </td>
                            <td>
                                <button onClick = { () => updateClass(item) } className = 'btn btn-warning'>Sửa</button>
                                <button onClick = { () => deleteClass(item._id) } className = 'btn btn-danger'>Xóa</button>
                            </td>
                        </tr>
                        )
                    }
            
                </table>
            </div>
          </Col>
        </Row>
      </Container>
    );
};

const mapStateToProps = state => ({
    classes : state.class.list,
    courses : state.course.data.allCourse,
    teachers : state.user.teachers,
    categories : state.category.data.allCategory
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(ClassList);