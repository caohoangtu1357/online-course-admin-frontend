import React, { Fragment, useEffect, useState } from 'react';
import './style.scss';
import { Col, Container, Row } from 'react-bootstrap';
import Menu from '../../component/menu/Menu';
import SideBar from '../../component/SideBar/SideBar';
import { useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import { GET_SPECIALIZES_BY_TRAINING_PROGRAM, GET_TRAINING_PROGRAM, GET_TRAINING_PROGRAM_COURSES, REMOVE_TRAINING_PROGRAM_COURSE } from '../../config/ActionTypes';
import TrainingProgramCourseModal from '../../component/training-program-course-modal';
import { formatDate } from '../../helper';

const TrainingProgramDetail = (props) => {

    const { id } = useParams();
    const { current } = props.trainingProgram;
    const [openModalAddCourse, setopenModalAddCourse] = useState(false);
    console.log(current);
    useEffect(() => {
        props.dispatch({
            type: GET_TRAINING_PROGRAM, 
            data : id
        });
        props.dispatch({
            type: GET_TRAINING_PROGRAM_COURSES,
            data: id
        });
        props.dispatch({ type: GET_SPECIALIZES_BY_TRAINING_PROGRAM, data: { id }})
    }, [ id ]);
   
    function removeCourse(data){
        const confirm = window.confirm('Bạn có chắc chắn muốn xóa khóa học này khỏi chương trình đào tạo ?');
        if(confirm){
            props.dispatch({
                type: REMOVE_TRAINING_PROGRAM_COURSE,
                data
            })
        }
        
    }

    return (
        <Container fluid>
            {
                openModalAddCourse && 
                <TrainingProgramCourseModal closeModal = {() => setopenModalAddCourse(false) } type = 'add' idTrainingProgram = { id } />
            }
        <Row className="wrapper">
          <Col xs={2} className="col">
            <SideBar/>
          </Col>
          <Col xs={10} className="col content-wrapper">
            <Menu/>
            <div className="main-content training-program-detail">
                <div className = 'info block '>
                    {
                        current._id && 
                        <Fragment>
                            <h4 className = 'title'>Thông tin về chương trình đào tạo</h4>
                            <h5><strong>Tên: </strong>{ current.name }</h5>
                            <h5><strong>Ngành: </strong> { current.idMajor.name }</h5>
                            <h5><strong>Khóa: </strong>{ current.idSchoolYear.name }</h5>
                            <h5><strong>Năm bắt đầu: </strong>{ current.idSchoolYear.year }  </h5>
                            <h5><strong>Ngày tạo: </strong> { formatDate(current.created_at) } </h5>
                        </Fragment>
                    }
                   
                </div>
                <div className = 'courses-info block'>
                    <h4 className = 'title'>Danh sách môn học</h4>
                    <button onClick = { () => setopenModalAddCourse(true) } className = 'btn-add-course btn btn-success'>
                        Thêm môn học
                    </button>
                    <table>
                        <tr>
                            <th>
                                STT
                            </th>
                            <th>
                                Tên
                            </th>
                            <th>
                                Mã môn
                            </th>
                            <th>
                                Loại
                            </th>
                            <th>
                                Trọng số
                            </th>
                            <th>
                                Số TC
                            </th>
                            <th>
                                Môn tiên quyết
                            </th>
                            <th>

                            </th>
                        </tr>
                        {
                            props.trainingProgram.courses.map((item,index) => 
                            <tr key = { item._id }>
                                <td>
                                    { index + 1 }
                                </td>
                                <td>
                                    { item.idCourse.name }
                                    {item.specialize &&
                                        <p className = 'specialize'>
                                            <strong>Chuyên ngành: </strong>
                                            {  item.specialize.name }
                                        </p>
                                    }
                                </td>
                                <td>
                                    { item.idCourse.code }
                                </td>
                                <td>
                                    { item.idCourse.type }
                                </td>
                                <td>
                                    { item.weight }
                                </td>
                                <td>
                                    { item.idCourse.credit }
                                </td>
                                <td>
                                    {
                                        item.isRequired && item.isRequired.name || 'Không'
                                    }
                                </td>
                                <td>
                                    <button onClick = { () => removeCourse({ idCourse: item.idCourse._id, idTrainingProgram: item.idTrainingProgram._id, index })} className = 'btn btn-danger'>
                                        Xóa
                                    </button>
                                </td>
                            </tr>
                            )
                        }
                      
                    </table>

                </div>

            </div>
          </Col>
        </Row>
      </Container>
    );
};


const mapStateToProps = state => ({
    trainingProgram: state.trainingProgram,
   
});

const mapDispatchToProps = dispatch => ({
    dispatch
});

export default connect(mapStateToProps,mapDispatchToProps)(TrainingProgramDetail);